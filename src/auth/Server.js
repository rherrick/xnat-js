export default class Server {
  constructor(
    address,
    username,
    password,
    alias = `${username}@${password}`,
    allowInsecureSsl = false
  ) {
    this.address = address;
    this.alias = alias;
    this.username = username;
    this.password = password;
    this.allowInsecureSsl = allowInsecureSsl;
    this.key = encodeServerKey(this.address, this.username);
  }
}

export function encodeServerKey(address, username) {
  return `${address}:${username}`.replace(/[:/@!.?&#]+/g, '_');
}
