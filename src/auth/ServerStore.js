import axios from 'axios';
import { URL } from 'url';
import { Agent } from 'https';
import store from 'store2';
import Server, { encodeServerKey } from './Server';

export default class ServerStore {
  constructor() {
    this.serverStore = store.namespace('xnat');
    this.instances = {};
  }

  getServers() {
    if (!this.serverStore.has('servers')) {
      this.serverStore.set('servers', []);
    }
    return this.serverStore.get('servers');
  }

  setServers(servers) {
    if (servers) {
      this.serverStore.set('servers', servers);
    } else {
      this.deleteServers();
      this.serverStore.set('servers', []);
    }
  }

  deleteServers() {
    this.serverStore.delete('servers');
  }

  getLastUsed() {
    return this.serverStore.get('lastUsed');
  }

  setLastUsed(lastUsed) {
    this.serverStore.set('lastUsed', lastUsed);
  }

  deleteLastUsed() {
    this.serverStore.delete('lastUsed');
  }

  getServer(key) {
    const servers = this.getServers();
    return servers.find(element => element.key === key);
  }

  saveServer(server) {
    const key = encodeServerKey(server.address, server.username);
    const found = this.getServer(key);
    if (found === undefined) {
      const servers = this.getServers();
      servers.push(server);
      this.setServers(servers);
    }
    return server;
  }

  saveServerProperties(address, alias, username, password, allowInsecureSsl = false) {
    return this.saveServer(
      new Server(address, username, password, alias, allowInsecureSsl)
    );
  }

  deleteServer(key) {
    const servers = this.getServers();
    const index = servers.findIndex(element => element.key === key);
    if (index >= 0) {
      servers.splice(index, 1);
      this.setServers(servers);
    }
  }

  loginPromise(server) {
    this.setLastUsed(server.key);

    const agent = ServerStore.getAgent(server);
    const instance = axios.create({
      httpsAgent: agent
    });
    this.instances[server.key] = instance;
    return instance.get(`${server.address}/data/auth`, {
      auth: {
        username: server.username,
        password: server.password
      }
    });
  }

  logoutPromise(server) {
    const instance =
      this.instances[server.key] ||
      axios.create({
        httpsAgent: new Agent({
          rejectUnauthorized: !server.allowInsecureSsl
        })
      });
    return instance.get(`${server.address}/app/action/LogoutUser`);
  }

  static getAgent(server) {
    const url = new URL(server.address);
    const port = url.port || (url.protocol === 'https:' ? 443 : 80);
    return new Agent({
      host: url.host,
      port,
      path: url.pathname,
      rejectUnauthorized: !server.allowInsecureSsl
    });
  }
}
