import ServerStore from '../../src/auth/ServerStore';

const store = new ServerStore();

describe('ServerStore', () => {
  it('has the same properties that were saved', () => {
    const server = store.saveServerProperties(
      'https://xnatdev.xnat.org',
      'Dev Admin',
      'admin',
      'admin',
      true
    );
    expect(server.address).toBe('https://xnatdev.xnat.org');
    expect(server.alias).toBe('Dev Admin');
    expect(server.username).toBe('admin');
    expect(server.password).toBe('admin');
    expect(server.allowInsecureSsl).toBe(true);
  });
});
